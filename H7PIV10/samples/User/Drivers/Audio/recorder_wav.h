#ifndef __RECORDER_WAV_H__
#define __RECORDER_WAV_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "sys.h"
/*********************************************
 * 
 * data struct
 * 
 * ******************************************/
typedef enum
{
  LittleEndian = 0,
  BigEndian
}Endianness_t;


typedef struct
{
  uint16_t  FormatTag;
  uint16_t  NumChannels;
  uint16_t  BlockAlign;
  uint16_t  BitsPerSample;
  uint32_t  RIFFchunksize;
  uint32_t  SampleRate;
  uint32_t  ByteRate;
  uint32_t  DataSize;
}wav_format_t;
	

/* Exported constants --------------------------------------------------------*/
#define  CHUNK_ID                            0x52494646  /* correspond to the letters 'RIFF' */
#define  FILE_FORMAT                         0x57415645  /* correspond to the letters 'WAVE' */
#define  FORMAT_ID                           0x666D7420  /* correspond to the letters 'fmt ' */
#define  DATA_ID                             0x64617461  /* correspond to the letters 'data' */
#define  FACT_ID                             0x66616374  /* correspond to the letters 'fact' */
#define  WAVE_FORMAT_PCM                     0x01
#define  FORMAT_CHNUK_SIZE                   0x10
#define  CHANNEL_MONO                        0x01
#define  CHANNEL_STEREO                      0x02
#define  SAMPLE_RATE_8000                    8000
#define  SAMPLE_RATE_11025                   11025
#define  SAMPLE_RATE_22050                   22050
#define  SAMPLE_RATE_44100                   44100
#define  BITS_PER_SAMPLE_8                   8
#define  BITS_PER_SAMPLE_16                  16



#ifdef __cplusplus
}
#endif	
	

#endif

