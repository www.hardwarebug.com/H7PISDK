#include "led.h"

/********************************************************
 *
 *LED ����״̬
 *
 ********************************************************/
void led(LED_Typedef num, uint8_t state)
{
	switch(num)
	{
		case LED_RED:
			HAL_GPIO_WritePin(LED_RED_PIN,(GPIO_PinState)state);
			break;
		case LED_GREEN:
			HAL_GPIO_WritePin(LED_GREEN_PIN,(GPIO_PinState)state);
			break;
		default:
			break;
	}
}