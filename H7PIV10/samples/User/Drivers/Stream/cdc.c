#include "cdc.h"

#include "string.h"
#include "stdarg.h"
#include "stdio.h"
 
cdc_t cdc;

uint8_t usb_cmds[][CDC_CMD_LENGTH]={
	"dfu",
	"reboot"
};
/***************************************************************************
清除接收buffer

****************************************************************************/
void cdc_clear_buffer(void)
{
	memset(cdc.rxBuffer,0,sizeof(cdc.rxBuffer));
	cdc.rxEnd = 0;
	cdc.rxLength = 0;
}
/***************************************************************************
cdc发送
uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len) 再usbd_cdc_if.c声明
****************************************************************************/


void cdc_send(uint8_t* Buf, uint32_t Len)
{
	#if USB_STACK_TYPE==USB_STACK_TYPE_ST
		CDC_Transmit_FS(Buf, Len);
	#elif USB_STACK_TYPE==USB_STACK_TYPE_TEENY
    tusb_cdc_device_send(&cdc_dev, Buf, Len);
	#endif
}

/***************************************************************************
echo 功能

****************************************************************************/

#if CDC_ECHO==1
void cdc_echo(uint8_t* Buf, uint16_t Len)
{
	char tmp[CDC_BUFFER_SIZE+6]={0};
	memset(tmp,0,sizeof tmp);
	sprintf(tmp,"echo> ");
	memcpy(tmp+6,Buf,Len);
	cdc_send((uint8_t*)tmp,Len+6);
}
#endif


/***************************************************************************
cdc接收
这个函数再static int8_t CDC_Receive_FS(uint8_t* Buf, uint32_t *Len)里默认调用

****************************************************************************/
void cdc_receive(uint8_t* Buf, uint16_t Len)
{
	#if CDC_ECHO==1
	cdc_echo(Buf,Len);
	#endif
	
	#if USB_STACK_TYPE==USB_STACK_TYPE_ST
		memcpy(cdc.rxBuffer,Buf,Len);
	#elif USB_STACK_TYPE==USB_STACK_TYPE_TEENY
    memcpy(cdc.rxBuffer,Buf,Len);
	#endif
	cdc.rxLength = Len;
	cdc.rxEnd = 1;
}


/***************************************************************************
系统日志打印
****************************************************************************/
void cdc_printf(const char *format, ...)
{
    va_list args;
    uint32_t length;
	  uint8_t buffer[CDC_BUFFER_SIZE];
 
    va_start(args, format);
    length = vsnprintf((char *)buffer, CDC_BUFFER_SIZE, (char *)format, args);
    va_end(args);
    tusb_cdc_device_send(&cdc_dev, buffer, length);
}




